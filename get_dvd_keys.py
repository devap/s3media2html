#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import boto3
import re

from debug2files import deBugData


### S3
s3r = boto3.resource('s3')
s3c = boto3.client('s3')
bucket = 'vihestorage'

### If the Bucket has over 1,000 items, we need to use a Paginator.
# Paginator holds 1000 objects at a time and will continue to repeat in chunks of 1000.
paginator = s3c.get_paginator('list_objects_v2')
pages = paginator.paginate(Bucket=bucket)


def get_s3_keys(pager):
  """get list of all files in s3://bucket """
  _s3keys = []
  for page in pager:
    for obj in page['Contents']:
      _s3keys.append(obj['Key'])
  print ('S3 keys have been retrieved... (', len(_s3keys), 'of them in fact!)')
  return _s3keys

def restore_object(key):
  'aws glacier restore-object --bucket bucket --key key'


if __name__ == '__main__':


  keys = get_s3_keys(pages)

  for key in keys:
    print ('key: ', key)

  #deBugData(keys, '01_raw_s3_keys')
