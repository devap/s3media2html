# -*- coding: utf-8 -*-

import os
import json
from collections import defaultdict

import logging
import logging.config
logging.config.fileConfig('logging.conf')


def deBugJSON(data):
  logger = logging.getLogger("JSON")
  logger.debug(data)

def deBugData(data, filename, debug_dir = 'debug'):
  _filename = ''.join([debug_dir, '/', filename])
  _typ = type(data)

  if not os.path.isdir(debug_dir):
    os.makedirs(debug_dir)

  if _typ is dict or _typ is defaultdict:
    _ofile = ''.join([_filename, '.json'])
    with open(_ofile, 'w') as fh:
      fh.write(json.dumps(data))

  elif _typ is list:
    _ofile = ''.join([_filename, '.list'])
    with open(_ofile, 'w') as fh:
      for line in data:
        if type(line) is tuple:
          fh.write(''.join([str(line), '\n']))
        else:
          if "\n" in line:
            fh.write(line)
          else:
            fh.write(''.join([line, '\n']))

  elif _typ is tuple:
    _ofile = ''.join([_filename, '.tuple'])
    with open(_ofile, 'w') as fh:
      fh.write(''.join([str(data), '\n']))

  elif _typ is str:
    with open(_ofile, 'w') as fh:
      fh.write(''.join([data, '\n']))

  else:
    return "Sorry, unknown data type - bailing out!"
