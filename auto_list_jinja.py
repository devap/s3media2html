#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
import copy

import json
import pickle
import uuid

from jinja2 import Environment, FileSystemLoader, select_autoescape
from urllib.parse import quote, quote_plus
from debug2files import deBugData, deBugJSON
from s3media import get_s3_keys, parse_s3_keys, sort_folders, pages, prefix

import logging
import logging.config
logging.config.fileConfig('logging.conf')
#logger = logging.getLogger("root")
logger = logging.getLogger("Adi")

#env = Environment(
#  loader=FileSystemLoader('templates', followlinks=True),
#  autoescape=select_autoescape(['html', 'xml'])
#)

env = Environment(loader=FileSystemLoader('templates'))

### media_url is used for generating the final URL to the object
media_uri = 'https://media.vihe.org/'
#media_uri = 'https://d1su4ik7pjeweo.cloudfront.net/'

ofile = '_s3_audio.inc'


def get_tracks(ary):
  if type(ary) != list:
    print ('\nUNEXPECTED: type(ary): ', type(ary) )
  _tracks = []
  for track in ary:
    iconClass = None
    trackLower = track["uri"].lower()
    if trackLower.endswith('.mp3'):
      iconClass = 'oi-play-circle'
    elif trackLower.endswith('.jpg'):
      iconClass = 'oi-image'
    elif trackLower.endswith('.doc'):
      iconClass = 'oi-book'
    elif trackLower.endswith('.pdf'):
      iconClass = 'oi-document'
    elif trackLower.endswith('.rtf'):
      iconClass = 'oi-document'
    elif trackLower.endswith('.ttf'):
      iconClass = 'oi-text'
    elif trackLower.endswith('.zip'):
      iconClass = 'oi-envelope-closed'

    _url = ''.join([prefix, track["uri"]])
    url_encoded = ''.join([media_uri, quote_plus(_url)])
    _track_obj = {
      "track": track["track"],
      "url": url_encoded,
      "icon": iconClass
    }
    _tracks.append(_track_obj)

  return _tracks



def cleanup_schema(ary):
  _folders_clean = []

  for folder in ary:
    _folder_obj = {
      "name": folder[0],
      "level1": [],
      "tracks": []
    }

    # start level1 loop
    for level1_key, level1_data in folder[1].items():
      _level1_obj = {}

      if level1_key:
        if level1_key == 'desc' and level1_data:
          # this description is for the folder
          _folder_obj["desc"] = level1_data
        elif level1_key == 'tracks':
          # we have folder level tracks
          _folder_obj["tracks"] = get_tracks(level1_data)
        else:
          _level1_obj["name"] = level1_key

      if level1_key != 'desc' and level1_key != 'tracks':
        # start level2 loop/extraction
        for level2_key, level2_data in level1_data.items():
          _level2_obj = {}

          """
            level1_key:  Japa and Holy Name Retreat
            level2_key:  Japa Retreat 2007
            level2_data: {'tracks': [{'track': '01DAY1-class.mp3', 'uri': ''}, {}, {} ] }
            level1_key:  Bhaktivinode Thakur's bhajans
            level2_key:  Gitavali
            level3_key:  01 Arunodaya-Kirtan
            level3_tracks: [{}, {}, {}, ...]
            level2_key:  KalyanaKalpataru
            level2_data: {'01 Intro and Desc... ', {'tracks': [{'track': '', 'uri': ""}, {}, {} ] }
            level2_key:  Saranagati
            level2_data: {'0 Introduction', { 'tracks': [{},{},{}] } }
            level2_key:  desc
            level2_data: 'All of Bhaktivinode Thakur's Sharanagati and Gitavali bhajans ...'
          """

          if level2_key:
            if level2_key == 'desc' and level2_data:
              _level1_obj["desc"] = level2_data
            elif level2_key == 'tracks':
              _level1_obj["tracks"] = get_tracks(level2_data)
            else:
              _level2_obj["name"] = level2_key

              # see if next level down contains tracks, or a level 3 album...
              for key, data in level2_data.items():
                if key == 'tracks':
                  _level2_obj["tracks"] = get_tracks(data)
                else:
                  _level3_obj = {
                    "name": key,
                    "tracks": get_tracks(data["tracks"])
                  }
                  if 'level3' not in _level2_obj:
                    _level2_obj["level3"] = []
                  _level2_obj["level3"].append(_level3_obj)

            if _level2_obj:
              if 'level2' not in _level1_obj:
                _level1_obj["level2"] = []
              _level1_obj["level2"].append(_level2_obj)


        #end of level2 loop
      if _level1_obj:
        _folder_obj["level1"].append(_level1_obj)

    _folders_clean.append(_folder_obj)

  _folders_all = {
    "folders": _folders_clean
  }

  return _folders_all


def adi():

  """Logging Based on http://docs.python.org/howto/logging.html#configuring-logging """

  _debug_dir='./0_debug'
  ifile = '000_raw_key_list.txt'
  ofile = '_s3_audio.inc'

  def get_keys(keyfile):
    _s3keys = []
    with open(keyfile, 'r') as fh:
      for line in fh:
        _s3keys.append(line)

    print ('S3 keys have been retrieved... (', len(_s3keys), 'of them in fact!)')
    return _s3keys


  #keys = get_keys(ifile)
  #deBugData(keys, '01_raw_s3_keys', debug_dir=_debug_dir)

  keys = get_s3_keys(pages)
  deBugData(keys, '01_raw_s3_keys', debug_dir=_debug_dir)

  data_raw = parse_s3_keys(keys)
  deBugData(data_raw, '02_parsed_s3_keys', debug_dir=_debug_dir)

  data_sorted = sort_folders(data_raw)
  deBugData(data_sorted, '03_folders_sorted', debug_dir=_debug_dir)

  data_clean = cleanup_schema(data_sorted)
  deBugJSON(data_clean)

  logger.debug(['data_clean:', data_clean])

  template = env.get_template('listings.html')
  markup = template.render( data_clean )

  with open(ofile, 'w') as fh:
    for line in markup:
      fh.write(line)

  print ("adi(DONE!): Please add a --debug flag")

  logger.info("Done!")

if __name__ == '__main__':

  logger = logging.getLogger("root")

  keys = get_s3_keys(pages)
  #deBugData(keys, '01_raw_s3_keys')

  data_raw = parse_s3_keys(keys)
  #deBugData(data_raw, '02_parsed_s3_keys')

  data_sorted = sort_folders(data_raw)
  #deBugData(data_sorted, '03_folders_sorted')

  data_clean = cleanup_schema(data_sorted)
  #deBugJSON(data_clean, '04_folders_cleaned')


  template = env.get_template('listings.html')
  markup = template.render( data_clean )

  with open(ofile, 'w') as fh:
    for line in markup:
      fh.write(line)

  print ("Please add a --debug flag!")
  print ("And a flag for generating Aindra's tracks?")

  sys.exit('Completed successfully!')
